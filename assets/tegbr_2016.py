import aiohttp
import asyncio
import io
import os
import requests

def extract_and_format_texts(data, language, heading_tag):
    return [f"<{heading_tag}>{item['text']}</{heading_tag}>" for item in data if "lang" in item and item["lang"] == language and "text" in item]

def extract_texts_in_order(data, language, article_id):
    texts = []
    for item in data:
        if "lang" in item and item["lang"] == language:
            if "text" in item:
                content_type = item.get("type")
                tag = "h4" if content_type == "subtitle" else "p"
                texts.append(f"<{tag}>{item['text']}</{tag}>")
            elif "image_path" in item:
                encoded_image_path = item['image_path'].replace("?", "%253F")
                image_url = (
                    f"https://www.businessreview.global/_next/image?url=https://d2du2xc4tgl2mb.cloudfront.net/article_images/{article_id}/{encoded_image_path}&w=1920&q=100"
                )
                replacements = {
                    "iimages": "images",
                    "//images": "/images",
                    "https://gbr.businessreview.global/store/2022-07-16_ingest/rjhn6n9ha0be506pcbqrpvif59c4j8bo/": "",
                    "https://gbr.businessreview.global/store/2024-02-17_ingest/08imijvigpugjgo210khs65mms8vqeiv/": "",
                }
                for key, value in replacements.items():
                    image_url = image_url.replace(key, value)
                texts.append(f'<a href="{image_url}" target="_blank" rel="noopener noreferrer"><img src="{image_url}" alt="{item.get("caption", "")}"/></a>')
    return texts

def fetch_subtitles(article_id, language_code):
    url = f"https://api.hummingbird.businessreview.global/api/article/index?id={article_id}"
    data = requests.get(url).json()
    subtitles = []
    for content in data["body"]["content"]:
        if content["type"] == "subtitle":
            for sub_data in content["data"]:
                if sub_data["lang"] == language_code:
                    subtitles.append(sub_data["text"])
    return subtitles

def replace_with_h4(html_content, subtitles):
    for subtitle in subtitles:
        html_content = html_content.replace(f"<p>{subtitle}</p>", f"<h4>{subtitle}</h4>")
    return html_content

def modify_html_content(html_content):
    html_content = html_content.replace("■", "")
    html_content += "<span>■</span></p>"
    html_content = html_content.replace("</p><span>", "<span>")
    html_content = html_content.replace("</h3>\n<span>■</span></p>", "</h3>")
    return html_content

async def fetch_article(session, sem, article_url, article_id, language_code):
    try:
        async with sem, session.get(article_url) as response:
            if response.status == 200:
                json_data = await response.json()
                fly_title_texts = extract_and_format_texts(json_data["body"]["fly_title"], language_code, "h2")
                title_texts = extract_and_format_texts(json_data["body"]["title"], language_code, "h1")
                rubric_texts = extract_and_format_texts(json_data["body"]["rubric"], language_code, "h3")

                all_texts = []
                for content_item in json_data["body"]["content"]:
                    if "data" in content_item:
                        all_texts.extend(extract_texts_in_order(content_item["data"], language_code, article_id))

                content = io.StringIO()
                content.write(f'<a href="https://www.businessreview.global/latest/{article_id}">❀</a>\n')
                content.write("\n".join(fly_title_texts + title_texts + rubric_texts) + "\n")
                content.write("\n".join(all_texts))

                zh_cn_subtitles = fetch_subtitles(article_id, language_code)
                modified_content = modify_html_content(content.getvalue())
                modified_content = replace_with_h4(modified_content, zh_cn_subtitles)

                return f"{modified_content}\n<div style='page-break-after: always;'></div>\n"
            else:
                print(f"Error: {response.status} for article {article_id}")
                return None
    except Exception as e:
        print(f"Error: {e} for article {article_id}")
        return None

async def main():
    year = "2016"
    use_api_data = not bool(year)
    
    if use_api_data:
        api_url = "https://api.hummingbird.businessreview.global/api/toc/get_articles"
        response = requests.get(api_url)

        if response.status_code == 200:
            json_data = response.json()
            article_ids = [art["article_id"] for art in json_data.get("articles", {}).get("new", [])]
        else:
            print(f"Error: Unable to fetch articles from api, status code: {response.status_code}")
            return
    else:
        file_path = f"assets/article_id/{year}.txt"
        if not os.path.exists(file_path):
            print(f"Error: File '{file_path}' not found.")
            return
        with open(file_path, "r") as file:
            article_ids = [line.strip() for line in file.readlines() if line.strip()]

    languages = ["en_GB", "zh_CN"]
    concatenated_content = '<h2>TEGBR</h2>\n<div style="page-break-before: always;"></div>\n'
    
    sem = asyncio.Semaphore(20)
    async with aiohttp.ClientSession() as session:
        for article_id in article_ids:
            tasks = [fetch_article(session, sem, f"https://api.hummingbird.businessreview.global/api/article/index?id={article_id}", article_id, lang) for lang in languages]
            results = await asyncio.gather(*tasks)
            concatenated_content += ''.join(results)

    output_path = "TEGBR_2016.html"
    with open(output_path, "w", encoding="utf-8") as output_file:
        output_file.write(concatenated_content)

    print(f"{output_path} created successfully.")

if __name__ == "__main__":
    asyncio.run(main())
